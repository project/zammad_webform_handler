<?php

namespace Drupal\zammad_webform_handler\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Zammad Webform Handler settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'zammad_webform_handler_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['zammad_webform_handler.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['authentication_method'] = [
      '#type' => 'radios',
      '#title' => $this->t('Authentication Method'),
      '#options' =>
    [
      'http-token' => $this->t('HTTP Token Validation'),
    ],
      '#default_value' => $this->config('zammad_webform_handler.settings')->get('authentication_method'),
      '#required' => TRUE,
    ];

    $form['zammad_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Zammad URL'),
      '#description' =>
      $this->t('The URL of your Zammad instance.'),
      '#default_value' => $this->config('zammad_webform_handler.settings')->get('zammad_url'),
      '#required' => TRUE,
    ];

    $form['http_token_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('HTTP Token'),
      '#description' =>
      $this->t('The HTTP token secret, used to access your zammad installation. Note: You need to allow Zammad API calls in your Zammad installation settings!'),
      '#default_value' => $this->config('zammad_webform_handler.settings')->get('http_token_secret'),
      '#states' => [
        'visible' => [
          ':input[name="authentication_method"]' => ['value' => 'http-token'],
        ],
      ],
    ];

    $form['timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Timeout for the Zamad Requests'),
      '#description' =>
      $this->t('Sets timeout for requests, defaults to 5 seconds, 0: no timeout.'),
      '#default_value' => $this->config('zammad_webform_handler.settings')->get('timeout'),
      '#required' => TRUE,
    ];

    $form['verify'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable SSL verification. Recommented for safe communication. Default: TRUE'),
      '#default_value' => $this->config('zammad_webform_handler.settings')->get('verify'),
    ];

    $form['sdk_link'] = [
      '#type' => 'link',
      '#title' => $this->t('For documentation see: Zammad API Client Docs'),
      '#url' => Url::fromUri('https://github.com/zammad/zammad-api-client-php'),
      '#attributes' => [
        'target' => '_blank',
      ]
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('zammad_webform_handler.settings')
      ->set('authentication_method', trim($form_state->getValue('authentication_method')))
      ->set('zammad_url', trim($form_state->getValue('zammad_url')))
      ->set('http_token_secret', $form_state->getValue('http_token_secret'))
      ->set('timeout', $form_state->getValue('timeout'))
      ->set('verify', $form_state->getValue('verify'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
