<?php

namespace Drupal\zammad_webform_handler\Plugin\WebformHandler;

use Drupal\Core\Url;
use ZammadAPIClient\Client;
use ZammadAPIClient\ResourceType;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Webform submission remote post handler.
 *
 * @WebformHandler(
 *   id = "zammad",
 *   label = @Translation("Zammad Ticket"),
 *   category = @Translation("External"),
 *   description = @Translation("Posts webform submissions to Zammad as new Ticket."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   tokens = TRUE,
 * )
 */
class ZammadWebformHandler extends WebformHandlerBase {

  /**
   * The token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $tokenManager;

  /**
   * The zammad_webform_handler config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $externalConfig;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->externalConfig = $container->get('config.factory')->get('zammad_webform_handler.settings');
    $instance->tokenManager = $container->get('webform.token_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'ticket_title' => '',
      'ticket_body' => '[webform_submission:values]',
      'ticket_group' => '',
      'ticket_type' => 'note',
      'ticket_customer_mail_simple' => '',
      // @todo Commented out, until
      // https://www.drupal.org/project/zammad_webform_handler/issues/3298775
      // is fixed.
      // 'ticket_priority_id' => '',
      // 'ticket_state_id' => '',
      // 'ticket_subject' => '',
      // 'ticket_internal' => FALSE,
      'ticket_customer_mail_simple' => '',
      'ticket_customer_mail_complex' => '',
      'additional_ticket_field_0' => [],
      'additional_ticket_field_1' => [],
      'additional_ticket_field_2' => [],
      'additional_ticket_field_3' => [],
      'additional_ticket_field_4' => [],
      'additional_ticket_field_5' => [],
      'additional_ticket_field_6' => [],
      'additional_ticket_field_7' => [],
      'additional_ticket_field_8' => [],
      'additional_ticket_field_9' => [],
      'user_creation_fallback' => 'simple-user',
      'user_first_name' => '',
      'user_second_name' => '',
      'user_web' => '',
      'user_telephone' => '',
      'user_mobile' => '',
      'user_fax' => '',
      'user_notes' => '',
      'additional_user_field_0' => [],
      'additional_user_field_1' => [],
      'additional_user_field_2' => [],
      'additional_user_field_3' => [],
      'additional_user_field_4' => [],
      'additional_user_field_5' => [],
      'additional_user_field_6' => [],
      'additional_user_field_7' => [],
      'additional_user_field_8' => [],
      'additional_user_field_9' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['zammad_webform_handler_settings'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Zammad Handler Settings'),
      '#description' => $this->t('Make sure the authentication method, the zammad url and the http token are set and make sure your zammad installation supports Zammad API Calls!'),
    ];
    $form['zammad_webform_handler_settings']['link'] = [
      '#type' => 'link',
      '#title' => $this->t('General Zammad settings page (API-Key / URLs)'),
      '#url' => Url::fromRoute('zammad_webform_handler.settings_form'),
      '#attributes' => [
        'class' => ['button'],
        'target' => '_blank',
      ],
    ];
    $form['zammad_webform_handler_settings']['required_ticket_settings'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Required Ticket Settings'),
    ];
    // @todo Commented out, until
    // https://www.drupal.org/project/zammad_webform_handler/issues/3298775
    // is fixed.
    // $form['zammad_webform_handler_settings']['optional_ticket_settings'] = [
    //   '#type' => 'details',
    //   '#open' => FALSE,
    //   '#title' => $this->t('Optional Ticket Settings'),
    // ];
    $form['zammad_webform_handler_settings']['additional_ticket_fields'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this->t('Additional Ticket Fields'),
      '#description' => $this->t('Define additional Zammad Ticket fields. Note, that you need to create the zammad ticket object first. The "Ticket Field Key" is the machine name of the Zammad user object and the "Ticket Field Value" the associated value. "Ticket Field Values" support tokens'),
    ];

    $form['zammad_webform_handler_settings']['user_creation_settings'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('User Creation Settings'),
    ];
    $form['zammad_webform_handler_settings']['api_link'] = [
      '#type' => 'link',
      '#title' => $this->t('Zammad API: Ticket Documentation'),
      '#url' => Url::fromUri('https://docs.zammad.org/en/latest/api/ticket/index.html'),
    ];

    // REQUIRED TICKET SETTINGS:
    $form['zammad_webform_handler_settings']['required_ticket_settings']['ticket_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Zammad Ticket Title'),
      '#default_value' => $this->configuration['ticket_title'],
      '#description' => $this->t('The Zammad Ticket Title text, e.g. "[webform_submission:values:subject] ([webform:title] - [site:url])"') . ' ' . $this->t('This field supports tokens.'),
      '#required' => TRUE,
    ];
    $form['zammad_webform_handler_settings']['required_ticket_settings']['ticket_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Zammad Ticket Body'),
      '#default_value' => $this->configuration['ticket_body'],
      '#description' => $this->t('The Zammad Ticket Body text, e.g. [webform_submission:values:message]') . ' ' . $this->t('This field supports tokens.'),
      '#required' => TRUE,
    ];
    $form['zammad_webform_handler_settings']['required_ticket_settings']['ticket_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Zammad Ticket Type'),
      '#default_value' => $this->configuration['ticket_type'],
      '#options' => [
        'email' => $this->t('E-Mail'),
        'phone' => $this->t('Phone'),
        'web' => $this->t('Web'),
        'note' => $this->t('Note'),
        'sms' => $this->t('SMS'),
        'chat' => $this->t('Chat'),
        'fax' => $this->t('Fax'),
      ],
      '#description' => $this->t('The Zammad Ticket Type.') . ' ' . $this->t('This field supports tokens.'),
      '#required' => TRUE,
    ];
    $form['zammad_webform_handler_settings']['required_ticket_settings']['ticket_group'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Zammad Ticket Group'),
      '#default_value' => $this->configuration['ticket_group'],
      '#description' => $this->t('The Zammad Group, e.g. "Sales"') . ' ' . $this->t('This field supports tokens.'),
      '#required' => TRUE,
    ];

    // OPTIONAL TICKET SETTINGS:
    // @todo Commented out, until
    // https://www.drupal.org/project/zammad_webform_handler/issues/3298775
    // is fixed.
    // $form['zammad_webform_handler_settings']['optional_ticket_settings']['ticket_subject'] = [
    //   '#type' => 'textfield',
    //   '#title' => $this->t('Zammad Ticket Subject'),
    //   '#default_value' => $this->configuration['ticket_subject'],
    //   '#description' => $this->t('The ticket subject. Note, that this differs from the ticket title, as it can be edited in the Zammad Interface! This field supports tokens.'),
    // ];
    // $form['zammad_webform_handler_settings']['optional_ticket_settings']['ticket_priority_id'] = [
    //   '#type' => 'textfield',
    //   '#title' => $this->t('Zammad Ticket Priority ID'),
    //   '#description' => $this->t('The ticket priority: 1 (low priority) - 3 (high priority). This field supports tokens.'),
    //   '#default_value' => $this->configuration['ticket_priority_id'],
    // ];
    // $form['zammad_webform_handler_settings']['optional_ticket_settings']['ticket_state_id'] = [
    //   '#type' => 'textfield',
    //   '#title' => $this->t('Zammad Ticket State ID'),
    //   '#description' => $this->t('The ticket state id:<br>
    //   <ul>
    //     <li>1 => new</li>
    //     <li>2 => open</li>
    //     <li>3 => closed</li>
    //     <li>4 => pending close</li>
    //     <li>5 => pending reminder</li>
    //   </ul><br>This field supports tokens.'),
    //   '#default_value' => $this->configuration['ticket_state_id'],
    // ];
    // $form['zammad_webform_handler_settings']['optional_ticket_settings']['ticket_internal'] = [
    //   '#type' => 'checkbox',
    //   '#title' => $this->t('Whether or not it is an internal ticket.'),
    //   '#default_value' => $this->configuration['ticket_internal'],
    // ];
    $form['zammad_webform_handler_settings']['additional_ticket_fields']['table'] = [
      '#type' => 'table',
      '#header' => [
        'additional_ticket_field_key' => $this->t('Ticket Field Key'),
        'additional_ticket_field_value' => $this->t('Ticket Field Value'),
      ],
    ];

    for ($i = 0; $i < 10; $i++) {
      // Since we use an array to associate the custom ticket field key with the
      // value, we need to get the key of the first (and only) array entry here:
      $ticketFieldKey = array_key_first($this->configuration['additional_ticket_field_' . $i]);
      $form['zammad_webform_handler_settings']['additional_ticket_fields']['table']['additional_ticket_field_' . $i]['additional_ticket_field_key'] = [
        '#type' => 'textfield',
        '#default_value' => !empty($ticketFieldKey) ? $ticketFieldKey : '',
      ];
      $form['zammad_webform_handler_settings']['additional_ticket_fields']['table']['additional_ticket_field_' . $i]['additional_ticket_field_value'] = [
        '#type' => 'textfield',
        '#default_value' => !empty($ticketFieldKey) ? $this->configuration['additional_ticket_field_' . $i][$ticketFieldKey] : '',
      ];
    }

    // USER CREATION SETTINGS:
    $form['zammad_webform_handler_settings']['user_creation_settings']['user_creation_fallback'] = [
      '#type' => 'radios',
      '#title' => $this->t('User Creation Fallback'),
      '#options' => [
        'simple-user' => $this->t('Simple user | User identification via Mail (A simple zammad user gets created, when the mail does not exist.)'),
        'complex-user' => $this->t('Complex user | User identification via Mail (A complex zammad user gets created, when the mail does not exist.'),
      ],
      '#default_value' => $this->configuration['user_creation_fallback'],
      '#description' => $this->t('The user creation fallback if no user with the chosen identification measure exists.'),
      '#required' => TRUE,
    ];


    // Simple User:
    $form['zammad_webform_handler_settings']['user_creation_settings']['ticket_customer_mail_simple'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Create ticket on behalf of customer mail adress (Creating a <b>simple</b> zammad user if mail not found)'),
      '#description' => $this->t('A simple zammad user only has a mail adress set.') . ' ' . $this->t('This field supports tokens.'),
      '#default_value' => $this->configuration['ticket_customer_mail_simple'],
      '#states' => [
        'visible' => [
          ':input[name="settings[user_creation_fallback]"]' => ['value' => 'simple-user'],
        ],
        'required' => [
          ':input[name="settings[user_creation_fallback]"]' => ['value' => 'simple-user'],
        ],
      ],
    ];

    // Complex User:
    $form['zammad_webform_handler_settings']['user_creation_settings']['ticket_customer_mail_complex'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Create ticket on behalf of customer mail (Creating a <b>complex</b> zammad user if mail not found)'),
      '#description' => $this->t('A complex zammad user can have multiple profile informations set. He consist of atleast a mail adress') . ' ' . $this->t('This field supports tokens.'),
      '#default_value' => $this->configuration['ticket_customer_mail_complex'],
      '#states' => [
        'visible' => [
          ':input[name="settings[user_creation_fallback]"]' => ['value' => 'complex-user'],
        ],
        'required' => [
          ':input[name="settings[user_creation_fallback]"]' => ['value' => 'complex-user'],
        ],
      ],
    ];

    // Standard User Fields:
    $form['zammad_webform_handler_settings']['user_creation_settings']['standard_user_fields'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Standard User Fields'),
      '#states' => [
        'visible' => [
          ':input[name="settings[user_creation_fallback]"]' => ['value' => 'complex-user'],
        ],
        'required' => [
          ':input[name="settings[user_creation_fallback]"]' => ['value' => 'complex-user'],
        ],
      ],
    ];

    $form['zammad_webform_handler_settings']['user_creation_settings']['standard_user_fields']['user_first_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User First Name'),
      '#description' => $this->t('The Zammad Account First Name.') . ' ' . $this->t('This field supports tokens.'),
      '#default_value' => $this->configuration['user_first_name'],
    ];

    $form['zammad_webform_handler_settings']['user_creation_settings']['standard_user_fields']['user_second_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Second Name'),
      '#description' => $this->t('The Zammad Account Second Name.') . ' ' . $this->t('This field supports tokens.'),
      '#default_value' => $this->configuration['user_second_name'],
    ];

    $form['zammad_webform_handler_settings']['user_creation_settings']['standard_user_fields']['user_web'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Webpage'),
      '#description' => $this->t('The Zammad Account Webpage.') . ' ' . $this->t('This field supports tokens.'),
      '#default_value' => $this->configuration['user_web'],
    ];

    $form['zammad_webform_handler_settings']['user_creation_settings']['standard_user_fields']['user_telephone'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Telephone Number'),
      '#description' => $this->t('The Zammad Account Telephone Number.') . ' ' . $this->t('This field supports tokens.'),
      '#default_value' => $this->configuration['user_telephone'],
    ];

    $form['zammad_webform_handler_settings']['user_creation_settings']['standard_user_fields']['user_mobile'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Mobile Telephone Number'),
      '#description' => $this->t('The Zammad Account Mobile Telephone Number.') . ' ' . $this->t('This field supports tokens.'),
      '#default_value' => $this->configuration['user_mobile'],
    ];

    $form['zammad_webform_handler_settings']['user_creation_settings']['standard_user_fields']['user_fax'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Fax Number'),
      '#description' => $this->t('The Zammad Account Mobile Fax Number.') . ' ' . $this->t('This field supports tokens.'),
      '#default_value' => $this->configuration['user_fax'],
    ];

    $form['zammad_webform_handler_settings']['user_creation_settings']['standard_user_fields']['user_notes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Notes'),
      '#description' => $this->t('The Zammad Account Notes.') . ' ' . $this->t('This field supports tokens.'),
      '#default_value' => $this->configuration['user_notes'],
    ];

    // Additional User Fields:
    $form['zammad_webform_handler_settings']['user_creation_settings']['additional_user_fields'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this->t('Additional User Fields'),
      '#description' => $this->t('Define additional Zammad Account fields. Note, that you need to create the zammad user object first. The "User Field Key" is the machine name of the Zammad user object and the "User Field Value" the associated value. "User Field Values" support tokens'),
      '#states' => [
        'visible' => [
          ':input[name="settings[user_creation_fallback]"]' => ['value' => 'complex-user'],
        ],
        'required' => [
          ':input[name="settings[user_creation_fallback]"]' => ['value' => 'complex-user'],
        ],
      ],
    ];

    $form['zammad_webform_handler_settings']['user_creation_settings']['additional_user_fields']['table'] = [
      '#type' => 'table',
      '#header' => [
        'additional_user_field_key' => $this->t('User Field Key'),
        'additional_user_field_value' => $this->t('User Field Value'),
      ],
    ];

    for ($i = 0; $i < 10; $i++) {
      // Since we use an array to associate the custom user field key with the
      // value, we need to get the key of the first (and only) array entry here:
      $userFieldKey = array_key_first($this->configuration['additional_user_field_' . $i]);
      $form['zammad_webform_handler_settings']['user_creation_settings']['additional_user_fields']['table']['additional_user_field_' . $i]['additional_user_field_key'] = [
        '#type' => 'textfield',
        '#default_value' => !empty($userFieldKey) ? $userFieldKey : '',
      ];
      $form['zammad_webform_handler_settings']['user_creation_settings']['additional_user_fields']['table']['additional_user_field_' . $i]['additional_user_field_value'] = [
        '#type' => 'textfield',
        '#default_value' => !empty($userFieldKey) ? $this->configuration['additional_user_field_' . $i][$userFieldKey] : '',
      ];
    }

    $this->elementTokenValidate($form);

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (empty($this->externalConfig->get('authentication_method'))) {
      $form_state->setErrorByName('zammad_webform_handler_settings_link', $this->t('Please choose a authentication method on the settings page!'));
    }
    if (empty($this->externalConfig->get('zammad_url'))) {
      $form_state->setErrorByName('zammad_webform_handler_settings_link', $this->t('Please define a zammad url on the settings page!'));
    }
    if (empty($this->externalConfig->get('http_token_secret'))) {
      $form_state->setErrorByName('zammad_webform_handler_settings_link', $this->t('Please define a HTTP Token on the settings page!'));
    }
    // Form validation for additional ticket fields:
    for ($i = 0; $i < 10; $i++) {
      // Get the ticket field key from the table form:
      $additionalTicketFieldKey = $form_state->getValue([
        'zammad_webform_handler_settings',
        'additional_ticket_fields',
        'table',
        'additional_ticket_field_' . $i,
        'additional_ticket_field_key',
      ]);
      // Get the ticket field value from the table form:
      $additionalTicketFieldValue = $form_state->getValue([
        'zammad_webform_handler_settings',
        'additional_ticket_fields',
        'table',
        'additional_ticket_field_' . $i,
        'additional_ticket_field_value',
      ]);
      // Note the setErrorByName function looks weird, but is correct:
      if (!empty($additionalTicketFieldKey) && empty($additionalTicketFieldValue)) {
        $form_state->setErrorByName('zammad_webform_handler_settings][additional_ticket_fields][table][additional_ticket_field_' . $i . '][additional_ticket_field_value', $this->t('Please additionally define a Ticket Value!'));
      }
      if (empty($additionalTicketFieldKey) && !empty($additionalTicketFieldValue)) {
        $form_state->setErrorByName('zammad_webform_handler_settings][additional_ticket_fields][table][additional_ticket_field_' . $i . '][additional_ticket_field_key', $this->t('Please additionally define a Ticket Key!'));
      }
    }

    // Additional form validation for complex user creation:
    if ($form_state->getValue('user_creation_fallback') == 'complex-user') {
      for ($i = 0; $i < 10; $i++) {
        // Get the user field key from the table form:
        $additionalUserFieldKey = $form_state->getValue([
          'zammad_webform_handler_settings',
          'user_creation_settings',
          'additional_user_fields',
          'table',
          'additional_user_field_' . $i,
          'additional_user_field_key',
        ]);
        // Get the user field value from the table form:
        $additionalUserFieldValue = $form_state->getValue([
          'zammad_webform_handler_settings',
          'user_creation_settings',
          'additional_user_fields',
          'table',
          'additional_user_field_' . $i,
          'additional_user_field_value',
        ]);

        // Note the setErrorByName function looks weird, but is correct:
        if (!empty($additionalUserFieldKey) && empty($additionalUserFieldValue)) {
          $form_state->setErrorByName('zammad_webform_handler_settings][user_creation_settings][additional_user_fields][table][additional_user_field_' . $i . '][additional_user_field_value', $this->t('Please additionally define a User Value!'));
        }
        if (empty($additionalUserFieldKey) && !empty($additionalUserFieldValue)) {
          $form_state->setErrorByName('zammad_webform_handler_settings][user_creation_settings][additional_user_fields][table][additional_user_field_' . $i . '][additional_user_field_key', $this->t('Please additionally define a User Key!'));
        }
      }
    }
    // Delete all values of hidden elements here!
    switch ($form_state->getValue('user_creation_fallback')) {
      case 'complex-user':
        // Empty out mail for ticket_customer_mail_simple:
        $form_state->setValue([
          'zammad_webform_handler_settings',
          'user_creation_settings',
          'ticket_customer_mail_simple',
        ], '');
        break;

      case 'simple-user':
        // Empty out complex user mail:
        $form_state->setValue([
          'zammad_webform_handler_settings',
          'user_creation_settings',
          'ticket_customer_mail_complex',
        ], '');
        // Empty out standard_user_fields:
        $form_state->setValue([
          'zammad_webform_handler_settings',
          'user_creation_settings',
          'standard_user_fields',
        ], []);
        // Empty out additional_user_fields:
        $form_state->setValue([
          'zammad_webform_handler_settings',
          'user_creation_settings',
          'additional_user_fields',
        ], []);
        break;

      case 'none':
        // Empty out simple user mail:
        $form_state->setValue([
          'zammad_webform_handler_settings',
          'user_creation_settings',
          'ticket_customer_mail_simple',
        ], '');
        // Empty out complex user mail:
        $form_state->setValue([
          'zammad_webform_handler_settings',
          'user_creation_settings',
          'ticket_customer_mail_complex',
        ], '');
        // Empty out standard_user_fields:
        $form_state->setValue([
          'zammad_webform_handler_settings',
          'user_creation_settings',
          'standard_user_fields',
        ], []);
        // Empty out additional_user_fields:
        $form_state->setValue([
          'zammad_webform_handler_settings',
          'user_creation_settings',
          'additional_user_fields',
        ], []);
        break;

      default:
        throw new \Exception('Unknown user creation settings!');
    }
    return parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    // Additional form submition for complex user creation:
    for ($i = 0; $i < 10; $i++) {
      // Get the ticket field key from the table form:
      $additionalTicketFieldKey = $form_state->getValue([
        'zammad_webform_handler_settings',
        'additional_ticket_fields',
        'table',
        'additional_ticket_field_' . $i,
        'additional_ticket_field_key',
      ]);
      // Get the ticket field value from the table form:
      $additionalTicketFieldValue = $form_state->getValue([
        'zammad_webform_handler_settings',
        'additional_ticket_fields',
        'table',
        'additional_ticket_field_' . $i,
        'additional_ticket_field_value',
      ]);
      // Empty out old configuration:
      $this->configuration['additional_ticket_field_' . $i] = [];
      // Set key and value on the current additional user field:
      $this->configuration['additional_ticket_field_' . $i][$additionalTicketFieldKey] = $additionalTicketFieldValue;
    }

    // Additional form submition for complex user creation:
    if ($form_state->getValue('user_creation_fallback') == 'complex-user') {
      for ($i = 0; $i < 10; $i++) {
        // Get the user field key from the table form:
        $additionalUserFieldKey = $form_state->getValue([
          'zammad_webform_handler_settings',
          'user_creation_settings',
          'additional_user_fields',
          'table',
          'additional_user_field_' . $i,
          'additional_user_field_key',
        ]);
        // Get the user field value from the table form:
        $additionalUserFieldValue = $form_state->getValue([
          'zammad_webform_handler_settings',
          'user_creation_settings',
          'additional_user_fields',
          'table',
          'additional_user_field_' . $i,
          'additional_user_field_value',
        ]);
        // Empty out old configuration:
        $this->configuration['additional_user_field_' . $i] = [];
        // Set key and value on the current additional user field:
        $this->configuration['additional_user_field_' . $i][$additionalUserFieldKey] = $additionalUserFieldValue;
      }
    }
    $this->applyFormStateToConfiguration($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE): void {
    if ($this->configuration['user_creation_fallback'] == 'complex-user') {
      $this->sendZammadTicketComplexUser($webform_submission);
    }
    else {
      // Apply old logic for simple user / id:
      $this->sendZammadTicketSimpleUser($webform_submission);
    }
  }

  /**
   * Sends a Zammad Ticket.
   *
   * Sends a Zammad Ticket with alternate simple user creation using a
   * mail adress if the user does not exist.
   * Or no user creation using a given user id.
   */
  protected function sendZammadTicketSimpleUser(WebformSubmissionInterface $webform_submission): void {
    $client = new Client([
      'url' => $this->externalConfig->get('zammad_url'),
      'http_token' => $this->externalConfig->get('http_token_secret'),
      'timeout' => $this->externalConfig->get('timeout'),
      'verify' => $this->externalConfig->get('verify'),
    ]);

    $ticket = $client->resource(ResourceType::TICKET);

    // Tickets are always created on behalf of a user / customer.
    // Use "guess:"-functionality, if customer_id is empty:
    // See https://docs.zammad.org/en/latest/api/ticket/index.html:
    // If you want to create tickets on behalf other users,
    // use the customer_id attribute.
    // Use guess:{email address} to save an API call if you don’t know the users
    // ID or want to create the user in question.
    $customerMail = $this->replaceTokens($this->configuration['ticket_customer_mail_simple'], $webform_submission);

    $type = $this->configuration['ticket_type'];
    $group = $this->replaceTokens($this->configuration['ticket_group'], $webform_submission);
    $title = $this->replaceTokens($this->configuration['ticket_title'], $webform_submission);
    $body = $this->replaceTokens($this->configuration['ticket_body'], $webform_submission);

    // Setup required ticket data:
    $ticket_data = [
      'title' => $title,
      'group' => $group,
      'customer' => $customerMail,
      'article' => [
        'from' => $customerMail,
        'body' => $body,
        'type' => $type,
        'sender' => 'Customer',
      ],
    ];

    // Add optional ticket data:
    // @todo Commented out, until
    // https://www.drupal.org/project/zammad_webform_handler/issues/3298775
    // is fixed.
    // if (!empty($priorityId = $this->configuration['ticket_priority_id'])) {
    //   if (!empty($replacedPriorityId = $this->replaceTokens($priorityId, $webform_submission))) {
    //     $ticket_data['ticket_priority_id'] = $replacedPriorityId;
    //   }
    // }
    // if (!empty($stateId = $this->configuration['ticket_state_id'])) {
    //   if (!empty($replacedStateId = $this->replaceTokens($stateId, $webform_submission))) {
    //     $ticket_data['state_id'] = $replacedStateId;
    //   }
    // }
    // if (!empty($subject = $this->configuration['ticket_subject'])) {
    //   if (!empty($replacedSubject = $this->replaceTokens($subject, $webform_submission))) {
    //     $ticket_data['article']['subject'] = $replacedSubject;
    //   }
    // }
    // if ($internal = $this->configuration['ticket_internal']) {
    //   $ticket_data['article']['ticket_internal'] = $internal;
    // }
    for ($i = 0; $i < 10; $i++) {
      // Add additional ticket field data values:
      $ticketFieldKey = array_key_first($this->configuration['additional_ticket_field_' . $i]);
      if (!empty($ticketFieldKey)) {
        if (!empty($replacedData = $this->replaceTokens($this->configuration['additional_ticket_field_' . $i][$ticketFieldKey], $webform_submission))) {
          $ticket_data[$ticketFieldKey] = $replacedData;
        }
      }
    }
    // Set ticket values and send ticket:
    $ticket->setValues($ticket_data);
    $ticket->save();
    if ($ticket->hasError()) {
      $this->getLogger('zammad_webform_handler')
        ->error($ticket->getError());
    }
  }

  /**
   * Sends a Zammad Ticket.
   *
   * Sends a Zammad Ticket with alternate complex user creation using a
   * mail adress.
   */
  protected function sendZammadTicketComplexUser(WebformSubmissionInterface $webform_submission): void {

    $client = new Client([
      'url' => $this->externalConfig->get('zammad_url'),
      'http_token' => $this->externalConfig->get('http_token_secret'),
      'timeout' => $this->externalConfig->get('timeout'),
      'verify' => $this->externalConfig->get('verify'),
    ]);
    // Create User with given values:
    $user = $client->resource(ResourceType::USER);
    $customerMail = $this->replaceTokens($this->configuration['ticket_customer_mail_complex'], $webform_submission);

    $userDoesNotExist = empty($user->search('email:' . $customerMail));
    if ($userDoesNotExist) {
      $user_data = [
        'email' => $customerMail,
      ];

      // Add optional user data:
      if (!empty($data = $this->configuration['user_first_name'])) {
        if (!empty($replacedData = $this->replaceTokens($data, $webform_submission))) {
          $user_data['firstname'] = $replacedData;
        }
      }
      if (!empty($data = $this->configuration['user_second_name'])) {
        if (!empty($replacedData = $this->replaceTokens($data, $webform_submission))) {
          $user_data['lastname'] = $replacedData;
        }
      }
      if (!empty($data = $this->configuration['user_web'])) {
        if (!empty($replacedData = $this->replaceTokens($data, $webform_submission))) {
          $user_data['web'] = $replacedData;
        }
      }
      if (!empty($data = $this->configuration['user_telephone'])) {
        if (!empty($replacedData = $this->replaceTokens($data, $webform_submission))) {
          $user_data['phone'] = $replacedData;
        }
      }
      if (!empty($data = $this->configuration['user_mobile'])) {
        if (!empty($replacedData = $this->replaceTokens($data, $webform_submission))) {
          $user_data['mobile'] = $replacedData;
        }
      }
      if (!empty($data = $this->configuration['user_fax'])) {
        if (!empty($replacedData = $this->replaceTokens($data, $webform_submission))) {
          $user_data['fax'] = $replacedData;
        }
      }
      if (!empty($data = $this->configuration['user_notes'])) {
        if (!empty($replacedData = $this->replaceTokens($data, $webform_submission))) {
          $user_data['note'] = $replacedData;
        }
      }
      for ($i = 0; $i < 10; $i++) {
        // Add additional user field data values:
        $userFieldKey = array_key_first($this->configuration['additional_user_field_' . $i]);
        if (!empty($userFieldKey)) {
          if (!empty($replacedData = $this->replaceTokens($this->configuration['additional_user_field_' . $i][$userFieldKey], $webform_submission))) {
            $user_data[$userFieldKey] = $replacedData;
          }
        }
      }
      $user->setValues($user_data);
      $user->save();
      if ($user->hasError()) {
        $this->getLogger('zammad_webform_handler')
          ->error($user->getError());
        // Break here, if an error is thrown:
        return;
      }
    }
    $ticket = $client->resource(ResourceType::TICKET);
    $type = $this->configuration['ticket_type'];
    $group = $this->replaceTokens($this->configuration['ticket_group'], $webform_submission);
    $title = $this->replaceTokens($this->configuration['ticket_title'], $webform_submission);
    $body = $this->replaceTokens($this->configuration['ticket_body'], $webform_submission);
    // Setup required ticket data:
    $ticket_data = [
      'title' => $title,
      'group' => $group,
      'customer' => $customerMail,
      'article' => [
        'from' => $customerMail,
        'body' => $body,
        'type' => $type,
        'sender' => 'Customer',
      ],
    ];
    for ($i = 0; $i < 10; $i++) {
      // Add additional ticket field data values:
      $ticketFieldKey = array_key_first($this->configuration['additional_ticket_field_' . $i]);
      if (!empty($ticketFieldKey)) {
        if (!empty($replacedData = $this->replaceTokens($this->configuration['additional_ticket_field_' . $i][$ticketFieldKey], $webform_submission))) {
          $ticket_data[$ticketFieldKey] = $replacedData;
        }
      }
    }
    // @todo Commented out, until
    // https://www.drupal.org/project/zammad_webform_handler/issues/3298775
    // is fixed.
    // Add optional ticket data:
    // if (!empty($priorityId = $this->configuration['ticket_priority_id'])) {
    //   if (!empty($replacedPriorityId = $this->replaceTokens($priorityId, $webform_submission))) {
    //     $ticket_data['ticket_priority_id'] = $replacedPriorityId;
    //   }
    // }
    // if (!empty($stateId = $this->configuration['ticket_state_id'])) {
    //   if (!empty($replacedStateId = $this->replaceTokens($stateId, $webform_submission))) {
    //     $ticket_data['state_id'] = $replacedStateId;
    //   }
    // }
    // if (!empty($subject = $this->configuration['ticket_subject'])) {
    //   if (!empty($replacedSubject = $this->replaceTokens($subject, $webform_submission))) {
    //     $ticket_data['article']['subject'] = $replacedSubject;
    //   }
    // }
    // if ($internal = $this->configuration['ticket_internal']) {
    //   $ticket_data['article']['ticket_internal'] = $internal;
    // }
    // Set ticket values and send ticket:
    $ticket->setValues($ticket_data);
    $ticket->save();
    if ($ticket->hasError()) {
      $this->getLogger('zammad_webform_handler')
        ->error($ticket->getError());
    }
  }

}
